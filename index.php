<?php

  get_header();

    get_template_part('partials/_wrap-start');

      if (is_home() || is_archive('category') && !is_category('agenda') && !is_tax('segmento') && !is_tax('setores') && !is_tax('temas_e_mercados')) :
        get_template_part( 'contents/_loop-index' );

      elseif (is_tax('segmento') || is_tax('setores') || is_tax('temas_e_mercados')) :
        get_template_part( 'contents/_loop-tax' );

      elseif (is_category('agenda')) :
        get_template_part( 'contents/_loop-agenda' );

      elseif (is_singular('post')) :
        get_template_part( 'contents/_loop-single' );

      elseif (is_404()) :
        get_template_part( 'contents/_loop-404' );

      elseif (is_search()) :
        get_template_part( 'contents/_loop-search' );

      else :
        get_template_part( 'contents/_loop-page' );

      endif;

    get_template_part('partials/_wrap-end');
  
  get_footer();