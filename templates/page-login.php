<?php
  /* Template name: Login / Registro */
  get_header();
  get_template_part('partials/_wrap-start');

	if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>
	<article <?php post_class( 'page page--login' ); ?>>
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-6 col-xl-5 d-none d-lg-block">
          <div class="row">
            <figure class="m-0"><?php the_post_thumbnail( 'login-thumb', ['class' => 'img-fluid'] ); ?></figure>
          </div>
        </div>
        <div class="col-md-6 col-xl-7 d-flex align-items-center justify-content-center py-5 py-lg-0 mx-auto mx-lg-0">
          <div class="page page--login page--login__form">
            <?php the_title( '<h2 class="s-title s-title--big mb-3 mb-xl-5">', '</h2>' ); ?>

            <?php 
              if (is_page(188)) {
                get_template_part('partials/_login'); 
              } elseif (is_page(190)) {
                get_template_part('partials/_register'); 
              }
            ?>
            
          </div>
        </div>
      </div>
    </div>
	</article>
<?php 
  endwhile; 

  get_template_part('partials/_wrap-end');
  get_footer();
?>