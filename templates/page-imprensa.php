<?php
  /* Template name: Contatos */
  get_header();

  get_template_part('partials/_wrap-start');
  if (has_post_thumbnail()) {
    get_template_part( 'partials/_h-page' );
  } else {
    get_template_part( 'partials/_h-page-clean' );
  }

	if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>
	<article <?php post_class( 'page page--imprensa container mb-classic' ); ?>>
    <div class="row content">
      <div class="col-11 col-md-10 col-lg-7 mx-auto">
        <?php the_content(); ?>
      </div>
    </div>

    <footer class="row py-md-3">
      <div class="col-md-5 mx-auto d-flex flex-column align-items-center">
        <a class="phone-sebrae d-block" title="Ligue para nós!" href="tel:<?php $phone = get_field('phone_info', 'option'); echo $phone; ?>"><?php echo $phone; ?></a>
        <a href="http://sebraerj.com.br" target="_blank">www.sebraerj.com.br</a>
      </div>
    </footer>
	</article>
<?php 
  endwhile; 

  get_template_part('partials/_wrap-end');
  get_footer();
?>