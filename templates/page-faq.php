<?php
  /* Template name: FAQ */
  get_header();

  get_template_part('partials/_wrap-start');
  get_template_part( 'partials/_h-page' );

	if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>
	<article <?php post_class( 'page container mb-classic' ); ?>>
    <div class="content mb-classic">
      <?php if ( have_rows('block_tabs') ) : ?>
        <div class="row block--accordion">
          <div class="col-md-9 mx-auto">
            <div class="accordion" id="accordionFaq">
              <?php $i=1; while( have_rows('block_tabs') ) : the_row(); ?>
                <div class="card">
                  <div class="card-header" id="heading<?php echo $i; ?>">
                    <h2 class="mb-0">
                      <button class="btn btn-link text-left w-100" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="<?php echo ($i==1) ? 'true' : 'false' ; ?>" aria-controls="collapse<?php echo $i; ?>">
                        <?php the_sub_field('title_bt'); ?>
                      </button>
                    </h2>
                  </div>

                  <div id="collapse<?php echo $i; ?>" class="collapse <?php if ($i==1) { echo  'show'; } ?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordionFaq">
                    <div class="card-body">
                      <?php the_sub_field('content_bt'); ?>
                    </div>
                  </div>
                </div>               
              <?php $i++; endwhile; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>

    <footer class="row py-md-5">
      <div class="col-md-5 mx-auto d-flex flex-column align-items-center">
        <h3 class="s-title s-title--big mb-3 mb-md-5">Dúvidas?</h3>
        <a href="<?php echo get_permalink(22); ?>" class="btn btn--classic btn--classic__has-border font-ter w-100"><span>Entrar em contato</span></a>
      </div>
    </footer>
	</article>
<?php 
  endwhile; 

  get_template_part('partials/_wrap-end');
  get_footer();
?>