<?php
  /* Template name: Segmentos */
  get_header();

  get_template_part('partials/_wrap-start');
?>
	<div class="container block mb-classic pt-5">
    <div class="row justify-content-center">
      <div class="col-12 my-5">
        <?php the_title( '<h1 class="s-title s-title--clean">', '</h1>' ); ?>
        <?php the_content(); ?>
      </div>
      
      <?php 
        $terms = get_terms([
          'taxonomy' => 'segmento',
          'hide_empty' => false
        ]);
        if ($terms) :
          foreach ($terms as $term) :
            $thumb = get_field('image_category', 'term_'.$term->term_id);
      ?>
        <div class="col-md-3">
          <div class="block--term mb-4">
            <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" title="Saiba mais: <?php echo $term->name; ?>" aria-hidden="true" tabindex="-1" class="d-block thumb-effect">
              <img src="<?php echo $thumb['sizes']['post-thumb']; ?>" alt="<?php echo $term->name; ?>" class="img-fluid">
              <span><?php echo $term->name; ?></span>
            </a>
          </div>
        </div>
      <?php 
          endforeach;
        endif;
      ?>
    </div>
  </div>
<?php 
  get_template_part('partials/_wrap-end');
  get_footer();
?>