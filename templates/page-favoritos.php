<?php  
  
  /* Template name: Favoritos */
  get_header();

  $user_id  = wp_get_current_user()->ID;
  $posts    = get_user_favorites($user_id = null, $site_id = null, [ 'post_type' => ['post'] ]);

  get_template_part('partials/_wrap-start');

?>
	<div class="container">
    <div class="row">
      <div class="col-md-12 mx-auto">
        <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); } ?>
        <?php the_title( '<h2 class="s-title s-title--big mb-5">', '</h2>' ); ?>
        <div class="row">
          <div class="col-md-9">
            <div class="row">
              <?php            
                if ($posts) {
                  foreach ($posts as $post) {
                    setup_postdata($post);

                    echo '<div class="col-md-4">';
                      get_template_part( 'contents/_loop' );
                    echo '</div>';

                  }
                  wp_reset_postdata();
                }
              ?>
            </div>
          </div>
          <div class="col-md-3">
            <?php wp_nav_menu(['theme_location' => 'logged-sidebar', 'container'=> false, 'menu_class'=> 'block--sidebarnav']); ?>
          </div>          
        </div>
      </div>
    </div>
  </div>
<?php 

  get_template_part('partials/_wrap-end');
  get_footer();
?>