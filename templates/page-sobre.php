<?php
  /* Template name: Conteúdo em tabs */
  get_header();

  get_template_part('partials/_wrap-start');
  get_template_part( 'partials/_h-page' );

	if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>
	<article <?php post_class( 'page container mb-classic' ); ?>>
    <div class="content">
      <?php if ( have_rows('block_tabs') ) : ?>
        <div class="row block--tabs block--accordion">
          <div class="col-lg-3">
            <ul class="nav nav-tabs flex-column text-right" id="sisTab" role="tablist" aria-orientation="vertical">
              <?php $i=1; while( have_rows('block_tabs') ) : the_row(); ?>
                <li class="nav-item">
                  <a class="nav-link text-uppercase <?php if ($i==1) { echo  'active'; } ?>" id="tab-<?php echo $i; ?>-link" data-toggle="tab" href="#tab-<?php echo $i; ?>" role="tab" aria-controls="tab-<?php echo $i; ?>" aria-selected="<?php echo ($i==1) ? 'true' : 'false' ; ?>">
                    <?php the_sub_field('title_bt'); ?>
                  </a>
                </li>
              <?php $i++; endwhile; ?>
            </ul>
          </div>

          <div class="col-lg-9">
            <div class="tab-content" id="sisTabContent">
              <?php $i=1; while( have_rows('block_tabs') ) : the_row(); ?>
                <div class="tab-pane fade <?php if ($i==1) { echo  'show active'; } ?>" id="tab-<?php echo $i; ?>" role="tabpanel" aria-labelledby="tab-<?php echo $i; ?>">
                  <?php the_sub_field('content_bt'); ?>
                </div>
              <?php $i++; endwhile; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
	</article>
<?php 
  endwhile; 

  get_template_part('partials/_wrap-end');
  get_footer();
?>