<?php
  /* Template name: Notificações */
  get_header();
  get_template_part('partials/_wrap-start');

	if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>
	<article <?php post_class( 'page page--login' ); ?>>
    <div class="container">
      <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); } ?>

      <div class="row">
        <div class="col-md-9">
          <div class="page page--edit">
            <div class="row">
              <div class="col-12 d-flex justify-content-between">
                <h2 class="s-title s-title--big mb-3 mb-xl-5">Notificações <br>por SMS/E-mail</h2>
                <button class="btn btn--classic btn--classic__outline no-shadow" id="edit-datas"><i class="icon icon-pencil mr-2"></i>Editar</button>
              </div>
            </div>

            <?php get_template_part('partials/_edit-notification'); ?>
          </div>
        </div>

        <div class="col-md-3">
          <ul class="terms-list block--sidebarnav">
            <?php 
              $pages = wp_list_pages([
                'child_of' => 282,
                'title_li' => ''
              ]);
            ?>
          </ul>
        </div>
      </div>
    </div>
	</article>
<?php 
  endwhile; 

  get_template_part('partials/_wrap-end');
  get_footer();
?>