<?php
  /* Template name: Busca */
  get_header();

  get_template_part('partials/_wrap-start');
?>
  <div class="container">
    <?php echo get_template_part( 'partials/_h-archive' ); ?>
    <div class="row">

      <div class="col-12 col-xl-10 mx-auto">        

        <div class="facetwp-template row">
          <?php 
            $args = ['facetwp' => true ];
            $query = new WP_Query($args); 
            if ($query->have_posts()):
              
              while ($query->have_posts()): $query->the_post();
            
              echo '<div class="col-md-6 col-lg-4">';
                get_template_part( 'contents/_loop' );
              echo '</div>';

              endwhile; 

            echo facetwp_display( 'pager' );

            else : 
              
              echo '<div class="col-12 mt-4">',
                      '<h2 class="s-title mb-4">Nenhum resultado encontrado.</h2>',
                      '<a href="#" onclick="FWP.reset()"> Tente novamente.</a>',
                    '</div>';

            endif; 
          ?>
        </div>

        <div class="default-loop row">
          <?php  echo get_template_part('partials/_loop-by-categories'); ?>
        </div> <!-- .row -->


      </div>
      
    </div>
</div>

<?php
  get_template_part('partials/_wrap-end');
  get_footer();
?>