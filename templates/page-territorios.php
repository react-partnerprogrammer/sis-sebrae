<?php
  /* Template name: Territorios */
  get_header();

  get_template_part('partials/_wrap-start');
  
	if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>  
  <article <?php post_class( 'container mb-classic' ); ?>>    
    <div class="row content">
      <div class="col-md-10 mx-auto">
        <header>
          <div class="h-page h-page--clean">
            <?php the_title('<h1>', '</h1>'); ?>
          </div>
          <div class="block--search__territorios">            
            <div class="row">
              <div class="col-md-12">
                <h2 class="font-classic">Busque notícias por cidades, regiões ou APL's:</h2>
              </div>
              <div class="col-md-10">
                <?php echo facetwp_display( 'facet', 'territorios' ); ?>
              </div>
              <div class="col-md-2">
                <button class="btn btn--classic w-100 my-3 my-md-0" onclick="FWP.refresh()"><span>Buscar</span></button>
              </div>
            </div>
          </div>
        </header>  
                
        <div class="row">
          <div class="col-12 mt-4"> 
            <br><br><br>
            <h2 class="s-title mb-4">Notícias Relacionadas</h2>
            <br>
          </div>
        </div>
        <div class="facetwp-template d-flex flex-wrap w-100" data-name="results">
          <?php 
            
            $args = ['cat' => 'noticias', 'facetwp' => true ];
            $query = new WP_Query($args); 
            if ($query->have_posts()):
              
              while ($query->have_posts()): $query->the_post();
                
                echo '<div class="col-md-6 col-lg-4">';
                  get_template_part( 'contents/_loop' );
                echo '</div>';
              
              endwhile;                      
            echo facetwp_display( 'pager' );

            else : 
              echo '<h2>Oops!</h2>';
            endif; 
          ?>
        </div>

      </div>
    </div>    
	</article>
<?php 
  endwhile; 

  get_template_part('partials/_wrap-end');
  get_footer();
?>