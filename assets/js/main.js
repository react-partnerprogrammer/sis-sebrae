(function ($, root, undefined) {
	$(function () {

    'use strict';    

    $('#sisTab').tabCollapse({
      tabsClass: 'd-none d-lg-flex',
      accordionClass: 'd-block d-lg-none'
    });

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    
    // accessibility control
    $( ".accessibility .dropdown" ).hover(
      function() {
        $('#dropdownLibras').dropdown('show');
      }, function() {
        $('#dropdownLibras').dropdown('hide');
      }
    );

    $('.accessibility button').on('click', function() {      
      $('html').removeClass('font-small font-normal font-large').addClass($(this).attr("class"));
    });
    $('.accessibility button.contrast').on('click', function() {      
      $('body').toggleClass('acessibilidade');
    });

    // search-box control
    $('.filter--toggle').click(function() {      
      $('.col-offcanvas, body').toggleClass('active');
      $('.slick-slider').addClass('slick-resizing');
      setTimeout(function() {        
        $('.slick-slider').slick('setPosition');
        $('.slick-slider').removeClass('slick-resizing');
      }, 600);
    });

    // inputs edit data control
    function lock_inputs() {
      $('.block-inputs input').attr('readonly', 'readonly');
      $('.btn-update').addClass('d-none');
      $('.btn-update').removeClass('d-flex');
    }

    function unlock_inputs() {
      $('.block-inputs input').attr('readonly', false);
      $('.btn-update').addClass('d-flex');
      $('.btn-update').removeClass('d-none');
    }

    lock_inputs();

    $('#edit-datas').click(function() {
      $(this).toggleClass('active');
      unlock_inputs();
    });

    // modal events control
    $(document).on('click', '.js-open-event', function (e) {
      e.preventDefault();
      
      let modal_id      = $(this).data('id'),
          modal         = $('#modal-event'),
          loading = $('.box-loading'),
          modal_content = $('#modal-event .content');

      loading.show();
      
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_object.ajaxurl,
        data: { action: 'get_event', id: modal_id },
        success: function(response){
          
          modal_content.html(response.html);
          loading.hide();
          modal.modal('show');          

        },
        error: function () {}
      });
    });

    $('#modal-event').on('hidden.bs.modal', function (e) {
      $('#modal-event .content').html('');
    });

    // $('.col-offcanvas').addClass('active');
    // $('body').addClass('active');

    // Disable auto refresh on all pages, except at search page
    if ('undefined' !== typeof FWP && $('body').hasClass('page-template-page-busca')) {
      FWP.auto_refresh = true;      
    } else if ('undefined' !== typeof FWP) {
      FWP.auto_refresh = false; 
    }
    
    
    $(document)    
    .on('ready', function(){
      if ('undefined' !== typeof FWP) FWP.refresh()

    })
    .on('facetwp-refresh', function(){
      // Adiciona a classe de loading ao recarregar os filtros
      if (FWP.loaded) {
        $('.facetwp-template').prepend('<div class=\"box-loading\"><div class=\"loading\"><div></div><div></div><div></div><div></div></div></div>');
      }
    })
    .on('facetwp-loaded', function() {      
      // Adiciona a classe facet--running se exister com algum filtro ativo
      $('body').toggleClass('facet-on', facetActive(FWP.facets));

      // Mantem o form do territorios atualizado com o novo layout
      updatefSelect($('.facetwp-facet[data-name=territorios] .fs-wrap'));      
    })
    
    // Mantem o form do territorios atualizado com o novo layout
    .on('fs:changed', function(e, $wrap){      
      updatefSelect($($wrap));
    })

    // Remove os itens do fSelect ao clicar no X
    .on('click', '.fs-label-remove', function(e){
      var value = $(e.target).data('value');
      var $wrap = $(e.target).closest('.fs-wrap');
      var fsOption = $wrap.find('.fs-dropdown [data-value='+value+']');
      fsOption.trigger('click');    
    })
    // Atualiza a busca ao clicar no ícone
    .on('click', '.facetwp-btn', function(){ 
      FWP.refresh(); 
    })
    // Altera a URL do site quando clicado na barra de busca da home 
    .on('click', '.fwp-submit__home', function(e){      
      e.preventDefault();
      FWP.refresh();      
      var searchUrl = '/busca/' + window.location.search.replaceAll('_select', '');
      // console.log(searchUrl);
      window.location.href = searchUrl;
    })

    
    // Verifica se existe algum facet ativo
    function facetActive(object) {
      for (var property in object) {
        if (object[property].length) 
          return true
      }
      return false;
    }

    // Atualiza o fSelect do facet
    function updatefSelect(els) {
      
      // Para cada $wrap de select contido na página
      if (els.length) {
        $.each(els, function (i, wrap) { 
           var $wrap = $(wrap);

            var selected = [];
            // Cria a key para podermos excluir futuramente
            $wrap.find('.fs-option.selected').each(function(i, el) {
              selected.push({key: $(el).data('value'), label: $(el).find('.fs-option-label').html()})        
            });
            // console.log(selected);
            // Insere o novo template no wrap
            $wrap.find('.fs-label').html(
              selected.map(function(item) {
                return `<span><span class="fs-label-title">${item.label}</span><span class="fs-label-remove" data-value="${item.key}">X</span></span>`;
              })
            );      
            
        });
      }      
    }

    let selectTerritorios = $('.fSelect--territorios');

    if ( selectTerritorios.length ){
      selectTerritorios.fSelect({
        placeholder: '',
        numDisplayed: 3,
        overflowText: '{n} selecionado',
        noResultsText: 'Nenhum resultado encontrado',
        searchText: 'Pesquisar',
        showSearch: true
      });
    }    
    
		// form control
		$("#search-box input, .wpcf7-form input, .wpcf7-form textarea").focus(function() {
  		$(this).parent().siblings('label').addClass('has-value');
  		$(this).addClass('val');
  	}).blur(function() {
  		var text_val = $(this).val();
  		if(text_val === "") {
  			$(this).parent().siblings('label').removeClass('has-value');
  			$(this).removeClass('val');
  		}
  	});

  	$('.wpcf7-form .date').blur(function() {
	    if( $(this).val().length === 0 ) {
	      $(this).addClass('not-value');
	    }
  	});

	  // banners
	  $('.banner, .h-page--slider, .h-archive--slider').slick({
	  // $('.h-archive--slider').slick({
	    dots: true,
	    arrows: false,
			infinite: false,
			// fade: true,
      speed: 500,
      rows: 0,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay: true,
      autoplaySpeed: 7000,
      adaptiveHeight: true
    });

    // slider news
    $('.slider-dots').slick({
	    dots: true,
	    arrows: false,
			infinite: false,
			fade: true,
	    speed: 500,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 7000
	  });

	  // slider schedule
		$('.page--schedule__slider').slick({
			dots: false,
			arrows: true,
			infinite: false,
			speed: 500,
			rows: 2,
			slidesPerRow: 3,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesPerRow: 2
					}
				},
				{
					breakpoint: 552,
					settings: {
            slidesPerRow: 1,
            dots: true,
			      arrows: false
					}
				}
			]
    });
  
    // login/register control
    var formLogin       = $('#form-login'),
        formLoginStatus = $('#form-login  p.status');

    formLogin.on('submit', function(e) {
      formLoginStatus.show().text(ajax_object.loadingmessage);
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_object.ajaxurl,
        data: $(this).serialize(),
        success: function(data){
          formLoginStatus.text(data.message);

          // Se existir redirect_to
          if (ajax_object.redirect_to) {
            document.location.href = ajax_object.redirect_to;
          
            // Recarrega a página
          } else {
            document.location.reload();
          }

        },
        error: function () {}
      });
      e.preventDefault();
    });

    var formRegister  = $('#form-register'),
        formRegisterStatus = $('#form-register  p.status');

    formRegister.on('submit', function(e) {
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_object.ajaxurl,
        data: $(this).serialize(),
        success: function(data){
          formRegisterStatus.text(data.message);
          if (data.loggedin == true) {
            document.location.href = ajax_object.redirecturl;
          }
        },
        error: function () {}
      });
      e.preventDefault();
    });

    var formEdit  = $('#form-edit'),
        formEditStatus = $('#form-edit  p.status');

    formEdit.on('submit', function(e) {
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_object.ajaxurl,
        data: $(this).serialize(),
        success: function(data){
          formEditStatus.text(data.message);
          lock_inputs();
        },
        error: function () {}
      });
      e.preventDefault();
    });

    $('.js-ajaxcall').on('click', function(e){
      e.preventDefault();
      
      var $el = $(this);
      var icon = $el.find('i');

      // Add spinning class
      if (icon) { icon.addClass('fa fa-spin'); }
      // Disable the element
      $(this).addClass('disabled').prop( 'disabled', true );

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_object.ajaxurl,
        data : $(this).data(),
        success: function(response){
          console.log(response);

          if ($el.data('successText')) $el.text($el.data('successText'));
        },
        error: function (error) {
          console.log(error);
        }
      });
      e.preventDefault();

      // Remove spinning class
      if (icon) { icon.removeClass('fa fa-spin'); }
      // // Disable the element
      // $(this).removeClass('disabled').prop( 'disabled', false );

    });

    /* Favorites */
    /* ----------------------------------------- */
      $(document).on('favorites-updated-single', function(event, favorites, post_id, site_id, status){
        
        // Remove o item da página ao ser excluido dos conteúdos baixados
        if ($('body').hasClass('page-template-page-conteudos-baixados')) {
          var button = $('button[data-postid="' + post_id + '"]');          
          button.closest('.col-md-4').remove();
        }
      });      
    /* ----------------------------------------- Favorites */
    
    
    $(".toggle-password").click(function() {
      $(this).toggleClass("icon-invisible");
      var input = $('#password');
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    // bootstrap validator
    window.addEventListener('load', function() {
      var forms = document.getElementsByClassName('needs-validation');
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);


		/*  Default Scripts */
		/* ----------------------------------------- */
		// Mascara de DDD e 9 dígitos para telefones
		var SPMaskBehavior = function (val) {
			  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			}, spOptions = { onKeyPress: function(val, e, field, options) { field.mask(SPMaskBehavior.apply({}, arguments), options); } };
    $('.mask-phone, input[type="tel"]').mask(SPMaskBehavior, spOptions);
    
    $('.phone-sebrae').mask('0000 000 0000');
    $('.mask-cpf').mask('000.000.000-00', {reverse: true});
    $('.mask-money').mask("#.##0,00", {reverse: true});

		// SELECT , caso queira excluir algum elemento, colocar 'select:not(elementos)'
		$('select').not('.multiple').wrap('<div class="select-box"></div>');

		// Fancybox
		$("a[href$='.jpg'], a[href$='.png'], a[href$='.jpeg'], a[href$='.gif'], .fancybox").fancybox({
			loop : false,
		});

		// Rolagem suave
		$('a.smoothscroll').click(function() {
		  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		    var target = $(this.hash); target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		    if (target.length) { $('html,body').animate({ scrollTop: target.offset().top - 100 }, 1000); return false; }
		  }
		});
    /* -----------------------------------------  Default Scripts */
    
	});
})(jQuery, this);