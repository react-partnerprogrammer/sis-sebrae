<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
?>

	<footer class="main-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-3 mb-5 mb-md-0 text-center text-md-left">
          <h2>Acessibilidade</h2>
          <?php get_template_part('partials/_accessibility'); ?>
        </div>
        
        <div class="col-md-6 col-lg-3 text-center text-md-left mb-5 mb-lg-0">
          <h2>Atendimento</h2>
          <?php
            wp_nav_menu([
              'theme_location'  => 'atendimento',
              'container'       => 'div',
              'container_class' => 'justify-content-md-start',
              'menu_id'         => false,
              'menu_class'      => 'navbar-nav',
              'depth'           => 2,
              'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
              'walker'          => new wp_bootstrap_navwalker()
            ]);
          ?>
        </div>
        <div class="col-md-6 col-lg-3 text-center text-md-left mb-5 mb-md-0">
          <h2>Contato Sebrae</h2>
          <a class="phone-sebrae d-block" title="Ligue para nós!" href="tel:<?php $phone = get_field('phone_info', 'option'); echo $phone; ?>"><?php echo $phone; ?></a>
          <a href="http://sebraerj.com.br" target="_blank">www.sebraerj.com.br</a>
        </div>
        <div class="col-md-6 col-lg-3 text-center text-md-left">
          <h2>Redes Sociais</h2>
          <?php get_template_part('partials/_social-networks'); ?>
        </div>
			</div>
		</div>

		<div class="copy"></div>
  </footer>
  
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon icon-close"></i>
        </button>
        
        <div class="modal-body">
          <?php
            $loop = new WP_Query( ['page_id' => 188 ] );
            if ( $loop->have_posts() ) :
            while ( $loop->have_posts() ) : $loop->the_post();
          ?>
            <div class="media">
              <?php the_post_thumbnail( 'agenda-thumb', ['class' => 'img-fluid mr-5'] ); ?>

              <div class="media-body d-flex flex-column align-items-start">
                <h5 class="modal-title" id="loginModalLabel"><?php the_title( '<h3 class="s-title">', '</h3>' ); ?></h5>

                <?php get_template_part('partials/_login'); ?>
              </div>
            </div>
          <?php
            endwhile;
            wp_reset_postdata();
            endif;
          ?>
        </div>
      </div>
    </div>
  </div>

<?php 
  wp_footer(); 
  // Se for passado o parametro via redirect, favorita automaticamente
  if (isset($_GET['favoritar']) && $_GET['favoritar'] == 1) : ?>
    <script> 
      window.onload = function(){         
        document.getElementsByClassName("simplefavorite-button")[0].click(); 
      } 
    </script>
  <?php     
  endif;
?>
</body>
</html>