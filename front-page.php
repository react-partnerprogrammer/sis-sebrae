<?php
  get_header();

    get_template_part( 'partials/_slideshow' );
    get_template_part('partials/_wrap-start');

      get_template_part( 'partials/_block-search' );
      get_template_part( 'partials/_block-last-news' );
      get_template_part( 'partials/_block-schedule' );
      get_template_part( 'partials/_block-categories' );
  	
  	get_template_part('partials/_wrap-end');

  get_footer();