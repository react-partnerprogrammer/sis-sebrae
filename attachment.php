<?php
  
  // Se o usuário estiver logado
  if (is_user_logged_in()) {
    
    // Pega os conteúdos baixados
    $conteudos_baixados = get_user_favorites($user_id = null, $site_id = null, [ 'post_type' => ['attachment'] ]);
    // Se o attachament não for um conteúdo baixado
    if (!in_array($post->ID, $conteudos_baixados)) {
      // Salva o arquivo na área de conteúdos completos    
      $favorite = new \Favorites\Entities\Favorite\Favorite();    
      $favorite->update( $post->ID, 'active', get_current_blog_id() );          
    }
    
    // Redireciona o usuário para a URL do arquivo
    wp_redirect( $post->guid );
    exit;

  } else {

  get_header();
    get_template_part('partials/_wrap-start');
?>
   <div class="container mt-5 mb-classic">
    <div class="row">
      <div class="col-md-4 mx-auto">
        <h2 class="s-title s-title--big">Login</h2>
        <p>Para acessar o conteúdo é necessário que faça o login abaixo:</p>
        <?php 
          get_template_part( 'partials/_login' );
        ?>
      </div>
    </div>
  </div>
<?php 
    get_template_part('partials/_wrap-end');
  get_footer();
  }