<?php

// wrap item on menu
function pp_nav_wrap() {
  $wrap  = '<ul id="%1$s" class="%2$s">';
    $wrap .= '%3$s';
    $wrap .= '<li class="nav-item nav-item--search"><a href="'.get_site_url(null,'/busca').'" class="nav-link" data-box="search"><i class="icon icon-search"></i></a></li>';
  $wrap .= '</ul>';
  return $wrap;
}

// facet control
add_filter( 'facetwp_assets', function( $assets ) {
  unset($assets['front.css'] );
  
  return $assets;
});

// facetwp control
add_filter( 'facetwp_facet_dropdown_show_counts', '__return_false' );

// create form elements
function form_group($title, $facet, $columns) {
  
  $string = '<div class="form-group">';
    $string .= '<a class="btn-collapse" data-toggle="collapse" href="#'.sanitize_title( $title ).'" role="button" aria-expanded="false" aria-controls="estouBuscando">';
      $string .= $title. ' <i class="icon icon-angle-down ml-2"></i>';
    $string .= '</a>';

    $string .= '<div class="collapse columns--'.$columns.'" id="'.sanitize_title( $title ).'">';
        $string .= facetwp_display( 'facet', $facet );
    $string .= '</div>';
  $string .= '</div>';

  echo $string;
}


function ajax_register() {

  check_ajax_referer( 'ajax-register-nonce', 'security' );

  $info = [];
  $info['display_name']   = $info['first_name'] = $info['user_nicename'] = $_POST['name'];
  $info['user_login']     = sanitize_email( $_POST['email']);
  $info['user_pass']      = sanitize_text_field($_POST['password']);
  $info['user_email']     = sanitize_email( $_POST['email']);
 
  $user_register = wp_insert_user( $info );
  if ( is_wp_error($user_register) ) { 
    $error  = $user_register->get_error_codes();
  
    if(in_array('empty_user_login', $error)) :
      echo json_encode([ 'loggedin'=>false, 'message'=>__($user_register->get_error_message('empty_user_login')) ]);
    elseif(in_array('existing_user_login',$error)) :
      echo json_encode([ 'loggedin'=>false, 'message'=>__('Este usuário já existe em nossos cadastros.') ]);
    elseif(in_array('existing_user_email',$error)) :
      echo json_encode([ 'loggedin'=>false, 'message'=>__('Este e-mail já está registrado.') ]);
    else :
      auth_user_login($info['nickname'], $info['user_pass'], 'Registration');       
    endif;
  } else {
    echo json_encode([ 'loggedin' => true, 'message' => __('Sucesso, redirecionando...' ) ]);
  }

  die();
}
add_action( 'wp_ajax_nopriv_ajax_register', 'ajax_register' );
add_action( 'wp_ajax_ajax_register', 'ajax_register' );


// Login Control
function ajax_login() {
  check_ajax_referer( 'ajax-login-nonce', 'security' );

  // Define as informações de loogin enviadas
  $info = [
    'user_login' => $_POST['username'],
    'user_password' => $_POST['password'],
    'remember' => true,
  ];  
  
  // Define a resposta padrão a ser retornada
  $response = [
    'loggedin' => true,
    'message' => 'Sucesso, redirecionando...',
  ];

  // Se for enviado algum parametro de redirect, adiciona a resposta
  if (isset($_POST['redirect_to']) && $_POST['redirect_to']) {
    $response['redirect_to'] = $_POST['redirect_to'];
  }

  // Faz o login do usuario
  $login = wp_signon( $info, false );
    
  // Se o login falhar, altera a resposta padrão
  if ( is_wp_error($login) ){
    $response['message'] = 'Usuário e/ou senha incorretos';
    $response['loggedin'] = false;
  } 
  
  // Envia resposta
  wp_send_json($response);

}
add_action( 'wp_ajax_nopriv_ajax_login', 'ajax_login' );
add_action( 'wp_ajax_ajax_login', 'ajax_login' );

// Update User Control
function ajax_edit() {
  check_ajax_referer( 'ajax-edit-nonce', 'security' );

  // Captura informações do formulário
  $infos = [
    'ID'            => $_POST['ID'],
    'cpf'           => $_POST['cpf'],
    'phone'         => $_POST['phone'],
    'user_login'    => $_POST['username'],
    'user_password' => $_POST['password'],
    'enterprise'    => $_POST['enterprise'],
    'register'      => $_POST['register'],
    'cargo'         => $_POST['cargo'],
    'number'        => $_POST['number'],
    'date_open'     => $_POST['date_open'],
    'social_name'   => $_POST['social_name'],
    'cnae'          => $_POST['cnae']
  ];  
  
  // Define a resposta padrão a ser retornada
  $response = [
    'status' => 'success',
    'message' => 'Sucesso, seus dados foram atualizados!',
  ];

  // Faz a atualização dos dados
  $user = wp_update_user( $infos );

  // Atualiza as informações
  update_field( 'cpf_userdata', $infos['cpf'], 'user_' . $user );
  update_field( 'phone_userdata', $infos['phone'], 'user_' . $user );
  update_field( 'enterprise_userdata', $infos['enterprise'], 'user_' . $user );
  update_field( 'register_userdata', $infos['register'], 'user_' . $user );
  update_field( 'role_userdata', $infos['cargo'], 'user_' . $user );
  update_field( 'number_userdata', $infos['number'], 'user_' . $user );
  update_field( 'dateopen_userdata', $infos['date_open'], 'user_' . $user );
  update_field( 'socialname_userdata', $infos['social_name'], 'user_' . $user );
  update_field( 'cnae_userdata', $infos['cnae'], 'user_' . $user );

  // echo '<pre>'. print_r($response, 1) . '</pre>';
  // die();
    
  // Se o login falhar, altera a resposta padrão
  if ( is_wp_error( $user ) ){
    $response['message'] = 'Tente novamente';
    $response['status'] = 'failed';
  }
  
  // Envia resposta
  wp_send_json($response);

}
add_action( 'wp_ajax_nopriv_ajax_edit', 'ajax_edit' );
add_action( 'wp_ajax_ajax_edit', 'ajax_edit' );

// Get Event Control
function get_event() {
  $response = [];
  $post_id   = $_REQUEST['id'];

  // Carrego a data para formatá-la
  $start_date  = get_field('date_start_event', $post_id, false);
  $end_date    = get_field('date_end_event', $post_id, false);
  $date_string = "l, d \d\\e F";
  $hour_string = "H:i";
  $date        = date_i18n($date_string, strtotime($start_date));
  $hour        = date_i18n($hour_string, strtotime($end_date));
  $link_externo = get_field( 'url_content', $post_id );

  // carrego campos para validação
  $programming = get_field( 'programming_event', $post_id );
  $investment  = get_field( 'investment_event', $post_id );

  $response['html'] = '<div class="media">';
    $response['html'] .= get_the_post_thumbnail( $post_id, 'agenda-thumb', ['class' => 'img-fluid mr-5'] );

    $response['html'] .= '<div class="media-body d-flex flex-column align-items-start">';
      $response['html'] .= '<h5 class="s-title mb-0">'.get_the_title( $post_id ).'</h5>';
      $response['html'] .= '<p class="mb-2 mb-md-4 date">'. ucfirst($date) .' - '.$hour.'</p>';
      $response['html'] .= '<p class="mb-3 mb-md-5">'.get_field( 'desc_event', $post_id ).'</p>';
      
      if ($programming) {
        $response['html'] .= '<h6>Programação</h6>';
        $response['html'] .= '<p class="mb-3 mb-md-5">'.$programming.'</p>';
      }

      $response['html'] .= '<div class="row w-100 align-items-center">';
        if ($investment) {
          $response['html'] .= '<div class="col">';
            $response['html'] .= '<h6>Investimento</h6>';
            $response['html'] .= '<p class="investment">R$ <strong class="mask-money">'.$investment.'</strong></p>';
          $response['html'] .= '</div>';
        }

        $response['html'] .= '<div class="col pr-0 d-flex justify-content-end">';
          if ($link_externo) {
            $response['html'] .= '<a href="'.$link_externo['url'].'" target="_blank" class="btn--classic btn--classic__ter">Quero saber mais <i class="icon icon-angle-right ml-3"></i></a>';
          } elseif('' !== get_post($post_id)->post_content) {
            $response['html'] .= '<a href="'.get_permalink( $post_id ).'" class="btn--classic btn--classic__ter">Quero saber mais <i class="icon icon-angle-right ml-3"></i></a>';
          }
        $response['html'] .= '</div>';
        
      $response['html'] .= '</div>';
    $response['html'] .= '</div>';

  $response['html'] .= '</div>';
     
  wp_send_json($response);
}
add_action( 'wp_ajax_get_event', 'get_event' );
add_action( 'wp_ajax_nopriv_get_event', 'get_event' );


// populate type of post
function acf_load_category_field_choices( $field ) {
    
  // reset choices
  $field['choices'] = [];
  
  $choices = get_terms( 'category', [
    'hide_empty' => false
  ]);

  if( is_array($choices) ) {
    foreach( $choices as $choice ) {
      $field['choices'][ $choice->term_id ] = $choice->name;      
    }
  }
  
  return $field;
}
add_filter('acf/load_field/name=category', 'acf_load_category_field_choices');


// custom breadcrumbs for restrict area
function custom_breadcrumbs( $links ) {
  global $post;
  if (is_page(259)) {
    $links[1] = [
      'text' => 'Área Restrita'
    ];
    unset($links[0]);
  } elseif (is_page() && $post->post_parent == 282) {
    unset($links[0]);
    unset($links[2]);
  }

  return $links;
}
add_filter( 'wpseo_breadcrumb_links', 'custom_breadcrumbs' );


// add facetwp in main query
add_filter( 'facetwp_is_main_query', function( $is_main_query, $query ) {
  if ( '' !== $query->get( 'facetwp' ) ) {
    $is_main_query = (bool) $query->get( 'facetwp' );
  }
  return $is_main_query;
}, 10, 2 );

// facet-pager bootstrap style
add_filter( 'facetwp_pager_html', function( $output, $params ) {
  $output = '';
  $page = $params['page'];
  $total_pages = $params['total_pages'];

  $output .= '<nav aria-label="pagination">';
    $output .= '<ul class="pagination">';

      if ( 1 < $total_pages ) {
        if ( $page > 1 ) {
          $output .= '<li class="page-item first"><a class="facetwp-page page-link" data-page="' . ($page - 1) . '"><i class="icon icon-angle-left"></i></a></li>';
        }
        
        if ( 3 < $page ) {
          $output .= '<li class="page-item"><a class="facetwp-page page-link first-page" data-page="1">Primeira</a>';
        }
        if ( 1 < ( $page - 10 ) ) {
          $output .= '<li class="page-item"><a class="facetwp-page page-link" data-page="' . ($page - 10) . '">' . ($page - 10) . '</a></li>';
        }
        for ( $i = 2; $i > 0; $i-- ) {
          if ( 0 < ( $page - $i ) ) {
            $output .= '<li class="page-item"><a class="facetwp-page page-link" data-page="' . ($page - $i) . '">' . ($page - $i) . '</a></li>';
          }
        }

        $output .= '<li class="page-item active"><a class="facetwp-page page-link " data-page="' . $page . '">' . $page . '</a></li>';

        for ( $i = 1; $i <= 2; $i++ ) {
          if ( $total_pages >= ( $page + $i ) ) {
            $output .= '<li class="page-item"><a class="facetwp-page page-link" data-page="' . ($page + $i) . '">' . ($page + $i) . '</a></li>';
          }
        }
        if ( $total_pages > ( $page + 10 ) ) {
          $output .= '<li class="page-item"><a class="facetwp-page page-link" data-page="' . ($page + 10) . '">' . ($page + 10) . '</a></li>';
        }
        if ( $total_pages > ( $page + 2 ) ) {
          $output .= '<li class="page-item"><a class="facetwp-page page-link last-page" data-page="' . $total_pages . '">Última</a></li>';
        }

        if ( $page < $total_pages ) {
          $output .= '<li class="page-item"><a class="facetwp-page page-link" data-page="' . ($page + 1) . '"><i class="icon icon-angle-right"></i></a></li>';
        }
      }

    $output .= '</ul>';
  $output .= '</nav>';

  return $output;
}, 10, 2 );

// custom posts_per_page main query
// add_action( 'pre_get_posts', 'prefix_category_query' );
// function prefix_category_query( $query ) {
// 	if ( $query->is_main_query() && ! $query->is_feed() && ! is_admin() && is_category() ) {
// 		$query->set( 'posts_per_page', '2' );
// 	}
// }

// add_filter( 'facetwp_facet_html', function( $output, $params ) {
// 	if ( 'my_facet_name' == $params['facet']['territorios'] ) {
//     echo '<pre>'. print_r($output, 1) . '</pre>';
// 	  // $pattern = '/>([ ]+)([^<]+)/';
// 		// $output = preg_replace( $pattern, '>${2} ${1}', $output );
// 	}
// 	return $output;
// }, 10, 2 );

function custom_comments($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

  if ( 'div' == $args['style'] ) {
    $tag = 'div';
    $add_below = 'comment';
  } else {
    $tag = 'li';
    $add_below = 'div-comment';
  }
?>
  <<?php echo $tag ?>
  <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?>>
    <?php if ( 'div' != $args['style'] ) : ?>
      <div id="comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <?php if ( $comment->comment_approved == '0' ) : ?>
      <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br />
    <?php endif; ?>
    <div class="comment-meta">
      <?php 
      // echo '<pre>'.print_r($comment,1). '</pre>';
      // die();
        // $user = get_user_by('login', get_comment_author());
        // if ($user) {
          echo '<h4 class="name">'. ucwords($comment->comment_author) .'</h4>';
        // }
      
        comment_text();
        if (is_user_logged_in()) {
          comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );           
        }
      ?>
    </div>
  <?php if ( 'div' != $args['style'] ) : ?>
  </div>
  <?php endif; ?>
<?php
}


// control users pages
function is_user_page() {
  global $post;
  if ($post->ID === 282 || $post->post_parent === 282) {
    return true;
  } else {
    return false;
  } 

}

function user_notification() {
  global $post;
  if (!is_user_page()) return false;

  $user = wp_get_current_user(); 
  $user_id = $user->ID;

  $fields = get_fields('user_'.$user_id);

  $vazio = true;
  foreach ($fields as $field) {    
    if (!$field) {      
      $vazio = false;
      break;
    }
  }

  if (is_user_page() && !$vazio && !is_page(286)) {    
    echo  '<div class="container mt-5">',
            '<div class="alert alert-success alert-register" role="alert">',
              '<p>Mantenha seus dados atualizados! Complete o seu cadastro <a href="'.get_permalink(286).'">aqui</a></p>',
            '</div>',
          '</div>';
  }
}


// Filtra os CAMPOS VISIVEIS na edição de post baseado nos dados do usuário
add_filter('acf/fields/taxonomy/wp_list_categories/key=field_5c745bf5d0e2d', 'my_acf_validate_value', 10, 2); // category
add_filter('acf/fields/taxonomy/wp_list_categories/key=field_5c7458b89f5da', 'my_acf_validate_value', 10, 2); // setores
add_filter('acf/fields/taxonomy/wp_list_categories/key=field_5c745950ca064', 'my_acf_validate_value', 10, 2); // segmentos
add_filter('acf/fields/taxonomy/query/key=field_5c745978ca065', 'my_acf_validate_value', 10, 2); // temas_e_mercados
add_filter('acf/fields/taxonomy/query/key=field_5c74598dca066', 'my_acf_validate_value', 10, 2); // territorios


function my_acf_validate_value( $args, $field ) {
  
  // Pega o usuário atual
  $user = wp_get_current_user();
  // Se ele não for editor, sai do filtro
  if ($user && !in_array('editor', $user->roles)) return $args;
  
  $nome_do_acf = $field['_name'];
  // Busca os dados do usuario baseado no campo    
  $capacidades_user = get_field($nome_do_acf, 'user_' . $user->ID);

  if ($capacidades_user) {
    $args['include'] = $capacidades_user;
  }

  // return
  return $args;
  
}

add_filter('acf/fields/taxonomy/wp_list_categories', 'my_acf_validate_value', 10, 2);


// Limita a visualização dos posts baseado nos termos do USUário
function posts_for_current_editor($query) {
  global $pagenow;

  if( 'edit.php' != $pagenow || !$query->is_admin )
      return $query;
  
  // Pega o usuário atual
  $user = wp_get_current_user();
  // Se ele não for editor, sai do filtro
  if ($user && !in_array('editor', $user->roles)) return $args;

  // Taxonomias
  $taxonomies = [
    'category' => 'category',
    'setores' => 'setores', 
    'segmentos' => 'segmento', 
    'temas_e_mercados' => 'temas_e_mercados', 
    'territorios' => 'territorios', 
  ];
  
  $taxQuery = [
    'relation' => 'OR'
  ];
  foreach ($taxonomies as $acf_field_slug => $term_slug) {
    
    $user_terms = get_field($acf_field_slug, 'user_' . $user->ID);
    
    if ($user_terms) {
      
      $taxQuery[] = [
        'taxonomy' => $term_slug,
        'terms' => $user_terms,
      ];

    }
  }
  
  if (count($taxQuery) > 1) {
    $query->set('tax_query', $taxQuery);    
  }
  
  return $query;
}
add_filter('pre_get_posts', 'posts_for_current_editor');


function wpse218049_enqueue_comments_reply() {
  if( is_singular() && comments_open() && ( get_option( 'thread_comments' ) == 1) ) {
    wp_enqueue_script( 'comment-reply', 'wp-includes/js/comment-reply', array(), false, true );
  }
}
add_action(  'wp_enqueue_scripts', 'wpse218049_enqueue_comments_reply' );


/* Custom Roles Names */
/* ----------------------------------------- */
function wps_change_role_name() {
  global $wp_roles;
  if ( ! isset( $wp_roles ) )
      $wp_roles = new WP_Roles();
  
  // echo '<pre>'.print_r($wp_roles->roles,1). '</pre>';
  // die();
  $roles = [
    'subscriber' => 'Usuários',
    'editor' => 'Conteudistas Temáticos',
  ];
  if ($roles) {
    foreach ($roles as $slug => $name) {
      $wp_roles->roles[$slug]['name'] = $name;
      $wp_roles->role_names[$slug] = $name;                
    }
  }
}
add_action('init', 'wps_change_role_name');

// Remove Administrator role from roles list
add_action( 'editable_roles' , 'hide_adminstrator_editable_roles' );
function hide_adminstrator_editable_roles( $roles ){
  
  unset( $roles['wpseo_editor'] );    
  unset( $roles['wpseo_manager'] );    
  unset( $roles['contributor'] );    
  unset( $roles['author'] );    
  return $roles;
}

/* ----------------------------------------- Custom Roles Names */
