<?php



/**
 * Ajax Function to Update User
 *
 * @return void
 */
function pp_update_user_callback() {

  wp_send_json(($_REQUEST));

  $response   = [];
  $post_id    = $_REQUEST['post_id'];
  $favorites  = [];
  $user_id    = '';
  

  if ( $post_id && is_user_logged_in()) :
    $user_id      = wp_get_current_user()->ID;
    $favorites    = get_field('favorites', 'user_'.$user_id );
    $favorites[] = $post_id;

    update_field( 'favorites', $favorites, 'user_'.$user_id );
    
  endif;
  wp_send_json($response);
  die();
}
add_action( 'wp_ajax_nopriv_update_user', 'pp_update_user_callback' );
add_action( 'wp_ajax_update_user', 'pp_update_user_callback' );
