<article <?php post_class( 'loop--classic' ); ?>>
  <a href="<?php the_permalink(); ?>" title="Saiba mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1">
    <figure class="thumb-effect has-shadow">
      <?php
        the_post_thumbnail( 'post-thumb', ['class' => 'img-fluid w-100'] );

        if ( has_post_format('video') ) {
          echo '<span class="icon-format has-shadow"><i class="icon icon-video"></i></span>';
        } elseif ( has_post_format('audio') ) {
          echo '<span class="icon-format has-shadow"><i class="icon icon-audio"></i></span>';
        }
      ?>
    </figure>
  </a>
  <time class="mb-2"><?php the_time('j \d\e F'); ?></time>
  <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>
</article>