<?php 
  global $post; 
  $start = get_field('date_start_event', $post->ID, false);
  $end   = get_field('date_end_event', false, false);
  $start = new DateTime($start);
  $end   = new DateTime($end);
  $id    = $post->ID;
  $link_externo = get_field('url_content');
  
  if ($link_externo) {
    $url = $link_externo['url'];
    $target = isset($link_externo['target']) && $link_externo['target'] ? $link_externo['target'] : '_self';
  } elseif (isset($post->post_content) && !empty($post->post_content)) {
    $url = get_the_permalink();
  } else {
    $url = false;
  }
?>
<article <?php post_class( 'loop--classic loop--classic__date media has-shadow mb-4 align-items-center' ); ?> >
  <time class="mr-4 mr-md-2 mr-xl-5 ml-2"><strong><?php echo $start->format('j'); ?></strong><?php echo $start->format('M'); ?></time>

  <div class="media-body">
    <h4 class="entry-title">
      <a href="<?php echo $url ?>" class="js-open-event" data-id="<?php echo $id; ?>" title="Saiba mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1">
        <?php the_title(); ?>
      </a>        
    </h4>

    <p class="m-0"><?php echo $start->format('H:i').'h'; ?> - <?php echo $end->format('H:i').'h'; ?></p>
    
    <button type="button" class="ml-auto mr-0 more js-open-event" data-id="<?php echo $id; ?>">
      Saiba mais
    </button>
  </div>
</article>