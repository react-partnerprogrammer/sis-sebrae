<?php 
  $term_object = get_queried_object(); 

  $icon    = get_field('icon_category', 'term_'.$term_object->term_id);
  $thumb   = get_field('image_category', 'term_'.$term_object->term_id);
  $gallery = get_field('slider_category', 'term_'.$term_object->term_id);

  $next_sunday = strtotime('next sunday');
?>
<div class="page page--schedule mb-classic">
  <div class="container mt-5">
    <div class="row">
      <div class="col-md-10 mx-auto mb-2 mb-lg-5">
        <h2 class="s-title s-title--clean"><?php single_cat_title(); ?></h2>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto mb-5">
        <?php
          $loop = new WP_Query([
            'post_type'      => 'post', 
            'posts_per_page' => -1,
            'cat'            => [9]
          ]);
          if ( $loop->have_posts() ) :
            echo '<h2 class="s-title mb-4">Essa semana</h2>';

          echo '<div class="page--schedule__slider row">';
            while ( $loop->have_posts() ) : $loop->the_post();

              echo '<div class="col-md-6 col-lg-4">';

                get_template_part( 'contents/_loop-date' );

              echo '</div>';

            endwhile;
          echo '</div>';

          wp_reset_postdata();
          else :
            echo "<p><?php esc_html_e( 'Desculpe, não há nenhum evento cadastrado.' ); ?></p>";
          endif;
        ?>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto mb-5">
        <?php
          $loop = new WP_Query([
            'post_type'      => 'post', 
            'posts_per_page' => -1,
            'cat'            => [9]
          ]);
          if ( $loop->have_posts() ) :
            echo '<h2 class="s-title mb-4">Próxima semana</h2>';

          echo '<div class="page--schedule__slider row">';
            while ( $loop->have_posts() ) : $loop->the_post();

              echo '<div class="col-md-6 col-lg-4">';

                get_template_part( 'contents/_loop-date' );

              echo '</div>';

            endwhile;
          echo '</div>';

          wp_reset_postdata();
          else :
            echo "<p><?php esc_html_e( 'Desculpe, não há nenhum evento cadastrado.' ); ?></p>";
          endif;
        ?>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto mb-5">
        <?php
          $loop = new WP_Query([
            'post_type'      => 'post', 
            'posts_per_page' => -1,
            'cat'            => [9]
          ]);
          if ( $loop->have_posts() ) :
            echo '<h2 class="s-title mb-4">Esse mês</h2>';

          echo '<div class="page--schedule__slider row">';
            while ( $loop->have_posts() ) : $loop->the_post();

              echo '<div class="col-md-6 col-lg-4">';

                get_template_part( 'contents/_loop-date' );

              echo '</div>';

            endwhile;
          echo '</div>';

          wp_reset_postdata();
          else :
            echo "<p><?php esc_html_e( 'Desculpe, não há nenhum evento cadastrado.' ); ?></p>";
          endif;
        ?>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto mb-5">
        <?php
          $loop = new WP_Query([
            'post_type'      => 'post', 
            'posts_per_page' => -1,
            'cat'            => [9]
          ]);
          if ( $loop->have_posts() ) :
            echo '<h2 class="s-title mb-4">Próximo mês</h2>';

          echo '<div class="page--schedule__slider row">';
            while ( $loop->have_posts() ) : $loop->the_post();

              echo '<div class="col-md-6 col-lg-4">';

                get_template_part( 'contents/_loop-date' );

              echo '</div>';

            endwhile;
          echo '</div>';

          wp_reset_postdata();
          else :
            echo "<p><?php esc_html_e( 'Desculpe, não há nenhum evento cadastrado.' ); ?></p>";
          endif;
        ?>
      </div>
    </div>
  </div>
</div>

<?php  echo get_template_part('partials/_modal-date'); ?>
