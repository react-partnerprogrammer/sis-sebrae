<?php 
  $conteudo_completo = get_field('file_content');
  $post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
?>
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <article <?php post_class( 'page page--single mb-classic' ); ?> >
          
          <?php if (get_the_post_thumbnail()) : ?>            
            <header>
              <figure class="mb-5 has-mask">
                  <?php the_post_thumbnail( 'slide', array( 'class' => 'img-fluid mx-auto' ) ); ?>
              </figure>
            </header>
          <?php endif; ?>
          
					<div class="row">
            <div class="col-md-10 mx-auto">
              <div class="content mb-classic">
                <?php the_title( '<h1 class="s-title s-title--big mb-4">', '</h1>' ); ?>
                <?php the_content(); ?>
              </div>

              <div class="page--single__tags mb-classic">
                <p>
                  <?php 
                    $terms = [
                      'Conteúdo'         => 'category',  
                      'Setores'          => 'setores',  
                      'Segmentos'        => 'segmento', 
                      'Temas & Mercados' => 'temas_e_mercados',  
                      'Territórios'      => 'territorios'
                    ];

                    foreach ($terms as $title => $slug) {                    
                      echo '<span class="mr-2">'.get_the_term_list( $post->ID, $slug, $title.': ', ', ' ).'</span>';
                    }
                  ?>
                </p>
              </div>

              <footer class="d-flex flex-row justify-content-end flex-wrap">
                <?php the_favorites_button(); ?>
                <?php if ($conteudo_completo) :?>
                  <a href="<?php echo $conteudo_completo['link']; ?>" class="btn btn--classic" target="_blank">Acesse o conteúdo completo <i class="icon icon-angle-right ml-3"></i></a>
                <?php endif; ?>
                <?php get_template_part('partials/_share-buttons'); ?>

                <div class="page--single__comments w-100">
                  <?php comments_template(); ?>
                </div>
              </footer>
            </div>
          </div>

				</article>
			<?php endwhile; ?>
		</div>
	</div>
</div>