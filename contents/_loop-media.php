<article <?php post_class( 'loop--classic loop--classic__media media mb-4' ); ?> >
  <a href="<?php the_permalink(); ?>" class="mr-4" title="Saiba mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1">
    <figure class="thumb-effect m-0">
      <?php the_post_thumbnail( 'post-small', array( 'class' => 'img-fluid w-100' ) ); ?>
    </figure>
  </a>
  
  <div class="media-body">
    <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>
    <?php the_category(); ?>
  </div>
</article>