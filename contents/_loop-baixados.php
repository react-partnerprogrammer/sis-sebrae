<?php $cssClass = get_post_class( 'loop--classic', $post->post_parent ); ?>
<article <?php echo ' class="' . implode(' ', $cssClass) . '"' ?>>
  <a href="<?php the_permalink(); ?>" target="_blank" title="Saiba mais: <?php echo get_the_title($post->post_parent); ?>" aria-hidden="true" tabindex="-1">
    <figure class="thumb-effect has-shadow">
      <?php
        $thumbnail = get_the_post_thumbnail($post->post_parent, 'post-thumb', ['class' => 'img-fluid w-100'] );
        echo $thumbnail;

        if ( has_post_format('video', $post->post_parent) ) {
          echo '<span class="icon-format has-shadow"><i class="icon icon-video"></i></span>';
        } elseif ( has_post_format('audio', $post->post_parent) ) {
          echo '<span class="icon-format has-shadow"><i class="icon icon-audio"></i></span>';
        }
      ?>
    </figure>
  </a>
  <time class="mb-2"><?php echo get_the_time('j \d\e F', $post); ?></time>
  <h4 class="entry-title">
    <a href="<?php echo get_permalink() ?>" target="_blank" title="Saiba mais: <?php echo get_the_title($post->post_parent); ?>" >
      <?php echo get_the_title($post->post_parent); ?>
    </a>
  </h4>
  <?php the_favorites_button(); ?>
</article>