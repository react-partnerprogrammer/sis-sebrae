<div class="container">
	<div class="row">

    <div class="col-12">
      <?php get_template_part( 'partials/_h-archive' ); ?>
    </div>

    <div class="col-md-10 mx-auto">
      <div class="row">
        <div class="col-12">
          <h1 class="s-title s-title-h1"><?php single_cat_title(); ?></h1>
        </div>

        <div class="facetwp-template d-flex flex-wrap w-100">
          <?php 
            if ( have_posts() ): while( have_posts() ): the_post(); 
            
              echo '<div class="col-md-6 col-lg-4">';
                get_template_part( 'contents/_loop' );
              echo '</div>';

            endwhile; 

            echo facetwp_display( 'pager' );

            else : 
              get_template_part( 'contents/_loop' );

            endif; 
          ?>
        </div>
      </div>
    </div>
    
	</div>
</div>