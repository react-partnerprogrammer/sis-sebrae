
<div class="page page--term">
  <div class="container mt-5">
    <div class="row">
      
      <div class="col-md-9 mb-5">
        <?php echo get_template_part('partials/_h-terms') ?>
      </div>      
      
      <?php echo get_template_part('partials/_h-terms-gallery') ?>

      <div class="col-md-3 mb-5">
        <?php echo get_template_part('partials/_block-sidebarlinks') ?>        
      </div>
    </div>
  </div>
  
  <?php echo get_template_part('partials/_block-nextevents') ?>

  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto mb-classic">
        <div class="row">
          <?php echo get_template_part('partials/_loop-by-categories'); ?>
        </div>
      </div>
    </div>
  </div>
</div>
