<div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
      <?php 
        $image_size = apply_filters( 'wporg_attachment_size', 'large' ); 
          echo wp_get_attachment_image( get_the_ID(), $image_size ); 
      ?>
    </div>
  </div>
</div>