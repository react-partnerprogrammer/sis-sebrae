<article <?php post_class( 'loop--classic loop--classic__slider d-flex flex-column mx-auto align-items-center' ); ?> >
  <a href="<?php the_permalink(); ?>" title="Saiba mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1">
    <figure class="thumb-effect m-0">
      <?php the_post_thumbnail( 'post-medium', array( 'class' => 'img-fluid' ) ); ?>
    </figure>
  </a>
  
  <a href="<?php the_permalink(); ?>" title="Saiba mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1" class="btn btn--classic btn--classic__sec more">Saiba mais <i class="icon icon-angle-right ml-2"></i></a>
</article>