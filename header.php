<?php   
  // Se a página for área restrita, ou filha da área restrita, exibe login
  if (is_user_page() && !is_user_logged_in() && wp_redirect(get_my_login_url())) exit;
  
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link href="<?php images_url('favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- google maps -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1P-H_fyEh6IaGS_mdIAPnMUIiQhKON2s"></script>
    <?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

    <header class="main-header">
      <div class="main-header--topbar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-6 d-none d-md-block">
              <?php get_template_part('partials/_accessibility'); ?>
            </div>
            <div class="col-md-6 d-flex align-items-center justify-content-between justify-content-md-end pr-0">
              <a href="http://m.sebrae.com.br/sites/PortalSebrae/ufs/rj?codUf=20" target="_blank" class="d-block mr-3"><img src="<?php images_url('logo-sebrae.png'); ?>" alt="Sebrae" class="img-fluid"></a>

              <ul class="account">
                <?php 
                  if (is_user_logged_in()) { 
                  $current_user = wp_get_current_user();
                  $avatar = get_avatar( $current_user->ID, 24, $default, $alt, ['class' => 'rounded-circle mr-4'] );
                ?>
                  <li><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Sair</a></li>
                  <li>
                    <div class="btn-group">
                      <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $avatar, $current_user->user_firstname; ?>
                      </button>
                      <?php wp_nav_menu(['theme_location' => 'logged-user', 'container'=> false, 'menu_class'=> 'dropdown-menu dropdown-menu-right']); ?>                      
                    </div>
                  </li>
                <?php } else { ?>
                  <li><a href="<?php echo get_my_login_url() ?>">Login</a></li>
                  <li><a href="<?php echo get_permalink(190); ?>">Cadastre-se</a></li>
                <?php } ?>
                
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <nav id="navbar" class="navbar navbar-expand-menu bsnav py-2 px-0">
              <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <img src="<?php echo get_field('logo', 'option'); ?>" class="img-fluid" />
              </a>
              <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
              <?php
                wp_nav_menu([
                  'theme_location'  => 'global',
                  'container'       => 'div',
                  'container_class' => 'collapse navbar-collapse justify-content-md-end',
                  'menu_id'         => false,
                  'menu_class'      => 'navbar-nav navbar-mobile align-items-end',
                  'depth'           => 3,
                  'items_wrap'      => pp_nav_wrap(),
                  'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                  'walker'          => new wp_bootstrap_navwalker()
                ]);
              ?>
            </nav>

            <div class="bsnav-mobile">
              <div class="bsnav-mobile-overlay"></div>
              <div class="navbar"></div>
            </div>
          </div>
        </div>
      </div>
    </header>