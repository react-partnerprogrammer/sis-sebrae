<div class="container mb-classic block--schedule">
  <h1 class="s-title s-title--has-line">Agenda</h1>
  
  <div class="row">
    <?php
      $loop = new WP_Query( ['post_type' => 'post', 'posts_per_page' => 4 ] );
      if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
          echo '<div class="col-md-6 col-xl-3">';
            get_template_part('contents/_loop');
          echo '</div>';
        endwhile;
      wp_reset_postdata();
      else :
        echo '<p>'.esc_html_e( 'Desculpe, não há nenhum post cadastrado em agenda.' ).'</p>';
      endif;
    ?>
  </div>

  <div class="row justify-content-center">
    <a href="<?php echo get_permalink(); ?>" class="btn btn--classic">Ver mais <i class="icon icon-angle-right ml-3"></i></a>
  </div>
</div>