<?php
  $terms = get_terms([ 'taxonomy' => 'category', 'hide_empty'  => true, 'title_li'    => '']);
  $queriedObject = get_queried_object();
  if ($terms) {
    echo '<ul class="terms-list block--sidebarnav">';
      foreach ($terms as $term ) {
        echo '<li class="cat-item">',
                '<a href="#box-'.$term->slug.'" class="smoothscroll" title="Veja mais">',
                  $term->name,
                '</a>',
              '</li>';
      }
    echo '</ul>';
  }
?>