<form id="form-register" class="form-classic form-classic--account mb-classic" method="post" role="form">
  <p class="status"></p>

  <div class="form-group mb-3">
    <label for="name" class="normal">Nome</label>
    <input id="name" class="form-control" type="text" name="name">
  </div>

  <div class="form-group mb-3">
    <label for="email" class="normal">E-mail</label>
    <input id="email" class="form-control" type="email" name="email">
  </div>

  <div class="form-group mb-3 position-relative">
    <label for="password" class="normal">Senha</label>
    <input id="password" type="password" name="password" class="form-control">
    <span class="icon icon-visibility toggle-password"></span>
  </div>

  <div class="form-group mb-5">
    <label for="re-password" class="normal">Confirmar senha</label>
    <input id="re-password" type="password" name="re-password" class="form-control">
  </div>

  <input type="hidden" name="action" value="ajax_register" />
  <?php wp_nonce_field('ajax-register-nonce', 'security'); ?>

  <button type="submit" class="btn btn--classic submit_button w-100 mb-4 no-shadow">Cadastrar <i class="icon icon-angle-right ml-5"></i></button>

</form>