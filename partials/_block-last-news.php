<div class="container mb-classic">
  <div class="row">
    <div class="col-md-7">
      <h1 class="s-title s-title--has-line has-after">Últimas publicações</h1>
    </div>
  </div>
  <?php
    $loop = new WP_Query( ['post_type' => 'post', 'posts_per_page' => 3 ] );
    if ( $loop->have_posts() ) :
  ?>
    <div class="row">
      <div class="col-md-8 col-lg-7 col-xl-6 mr-auto">
        <?php
          $i=1; while ( $loop->have_posts() ) : $loop->the_post();

            get_template_part( 'contents/_loop-media' );

          $i++; endwhile;
        ?>
      </div>

      <div class="col-md-4 col-lg-5">
        <div class="slider-dots slick-slider--has-color">
        <?php
          $i=1; while ( $loop->have_posts() ) : $loop->the_post();

            get_template_part( 'contents/_loop-slider' );

          $i++; endwhile;
        ?>
        </div>
      </div>
    </div>
  <?php 
    wp_reset_postdata();
    endif;
  ?>

  <div class="row justify-content-center">
    <a href="<?php echo get_permalink(); ?>" class="btn btn--classic">Ver mais <i class="icon icon-angle-right ml-3"></i></a>
  </div>
</div>