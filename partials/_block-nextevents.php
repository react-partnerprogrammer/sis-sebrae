<?php 
  $queriedObject = get_queried_object(); 

  // Busca os próximos eventos baseado no termo buscado
  $loop = new WP_Query([
    'post_type'      => 'post', 
    'posts_per_page' => 3,
    'cat'            => [9],
    'tax_query'     => [
      'relation' => 'AND',
      [
        'taxonomy' => $queriedObject->taxonomy,
        'field'    => 'segmentos',
        'terms'    => [$queriedObject->slug],
      ]
    ]
  ]);
  
  if ( !$loop->have_posts() ) return;
?>
<div id="box-agenda" class="page--term__schedule inverse mb-classic mb-classic--big container-full">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-10 mx-auto">
          <div class="row">
            <?php
              
              if ( $loop->have_posts() ) :
                echo '<div class="col-12">',
                        '<h2 class="s-title mb-4 mt-3">Agenda de eventos</h2>',
                        '<div class="spacer pt-3"></div>',
                    '</div>';
              while ( $loop->have_posts() ) : $loop->the_post();

                echo '<div class="col-md-4 d-flex">';

                  get_template_part( 'contents/_loop-date' );

                echo '</div>';

              endwhile;
              wp_reset_postdata();
              else :
                echo "<p><?php esc_html_e( 'Desculpe, não há nenhum post cadastrado.' ); ?></p>";
              endif;
            ?>
          </div>
        </div>
      </div>

      <div class="row justify-content-center mt-4 mb-3">
        <a href="<?php echo get_busca_link([$queriedObject, get_category_by_slug('agenda')]) ?>" class="btn btn--classic btn--classic__ter"><span>Agenda completa <i class="icon icon-angle-right ml-3"></i></span></a>
      </div>
    </div>
  </div>

  <?php  echo get_template_part('partials/_modal-date'); ?>