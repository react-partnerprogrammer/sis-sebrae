<?php 
  /* 
  Arquivo utilizado para a exibição dos detalhes de um evento ao clicar em "Saiba Mais"
  */
?>
<div class="modal fade" id="modal-event" tabindex="-1" role="dialog" aria-labelledby="modalEventLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon icon-close"></i></button>

      <div class="modal-body content"></div>
    </div>
  </div>
</div>

<div class="box-loading" style="display: none;">
  <div class="loading"><div></div><div></div><div></div><div></div></div>
</div>