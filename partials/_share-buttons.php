<?php
	echo  '<div class="share-box mb-classic">',
					'<h4 class="d-flex align-items-center"><i class="icon icon-share mr-2"></i> Compartilhe!</h4>',
					do_shortcode('[easy-social-share counters=1 ]'),
				'</div>';
?>
