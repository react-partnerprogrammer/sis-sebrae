<form id="form-edit-notification" class="form-classic form-classic--account mb-classic" method="post" role="form">
  <p class="status"></p>

  <div class="row">

    <?php
      $setores = get_terms(['taxonomy' => 'setores', 'hide_empty' => false]);
      if ($setores) {
        echo '<div class="col-md-12">',
                '<div class="form-group mb-2 mb-xl-4 w-50">',
                  '<label for="setor" class="normal">Em qual setor você atua?</label>',
                  '<select class="custom-select">',
                    '<option value=""></option>';

                    foreach ($setores as $setor) {
                      echo '<option value="'.$setor->term_id.'">'.$setor->name.'</option>';
                    }

        echo      '</select>',
                '</div>',
              '</div>';
      }
    ?>

    <div class="col-md-12 mb-2">
      <label class="normal mb-3">Gostaria de receber informações sobre o Sebrae Inteligência Setorial?</label>
      <div class="form-group d-flex justify-content-md-start w-75">
        <div class="custom-control custom-checkbox flex-fill">
          <input class="custom-control-input" name="check-sms" type="checkbox" value="check-sms" id="check-sms">
          <label class="custom-control-label normal" for="check-sms">SMS</label>
        </div>
        <div class="custom-control custom-checkbox flex-fill">
          <input class="custom-control-input" name="check-email" type="checkbox" value="check-email" id="check-email">
          <label class="custom-control-label normal" for="check-email">E-mail</label>
        </div>
      </div>
    </div>

    <div class="col-12 mb-2">
      <label class="normal mb-3">Marque todos os setores de seu interesse e receba notificações quando novos produtos forem publicados:</label>
      <div class="form-group d-flex flex-wrap justify-content-md-start w-75">
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-boletins" type="checkbox" value="check-boletins" id="check-boletins">
          <label class="custom-control-label normal" for="check-boletins">Boletins de tendência</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-casos" type="checkbox" value="check-casos" id="check-casos">
          <label class="custom-control-label normal" for="check-casos">Casos de uso</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-relatorios" type="checkbox" value="check-relatorios" id="check-relatorios">
          <label class="custom-control-label normal" for="check-relatorios">Relatórios de inteligência</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-videos" type="checkbox" value="check-videos" id="check-videos">
          <label class="custom-control-label normal" for="check-videos">Vídeos</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-noticias" type="checkbox" value="check-noticias" id="check-noticias">
          <label class="custom-control-label normal" for="check-noticias">Notícias</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-poadcasts" type="checkbox" value="check-poadcasts" id="check-poadcasts">
          <label class="custom-control-label normal" for="check-poadcasts">Poadcasts</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-infograficos" type="checkbox" value="check-infograficos" id="check-infograficos">
          <label class="custom-control-label normal" for="check-infograficos">Infográficos</label>
        </div>
        <div class="custom-control custom-checkbox col-md-6">
          <input class="custom-control-input" name="check-agenda" type="checkbox" value="check-agenda" id="check-agenda">
          <label class="custom-control-label normal" for="check-agenda">Agenda</label>
        </div>
      </div>
    </div>

    <?php
      $setores = get_terms(['taxonomy' => 'setores', 'hide_empty' => false]);
      if ($setores) {
        echo '<div class="col-12 mb-2">',
                '<h4 class="mb-2 mb-md-3">Setores da economia</h4>',
                  '<div class="form-group d-flex flex-wrap justify-content-md-start w-75">';
                    foreach ($setores as $setor) {
                      echo '<div class="custom-control custom-checkbox col-md-6">',
                              '<input class="custom-control-input" name="'.$setor->term_id.'" type="checkbox" value="'.$setor->term_id.'" id="'.$setor->term_id.'">',
                              '<label class="custom-control-label normal" for="'.$setor->term_id.'">'.$setor->name.'</label>',
                            '</div>';
                    }
                    
        echo      '</div>',
              '</div>';
      }
    ?>

    <?php
      $temas = get_terms(['taxonomy' => 'temas_e_mercados', 'hide_empty' => false]);
      if ($temas) {
        echo '<div class="col-12 mb-2">',
                '<h4 class="mb-2 mb-md-3">Temas & Mercados</h4>',
                  '<div class="form-group d-flex flex-wrap justify-content-md-start w-75">';
                    foreach ($temas as $tema) {
                      echo '<div class="custom-control custom-checkbox col-md-6">',
                              '<input class="custom-control-input" name="'.$tema->term_id.'" type="checkbox" value="'.$tema->term_id.'" id="'.$tema->term_id.'">',
                              '<label class="custom-control-label normal" for="'.$tema->term_id.'">'.$tema->name.'</label>',
                            '</div>';
                    }
                    
        echo      '</div>',
              '</div>';
      }
    ?>

    <?php
      $segmentos = get_terms(['taxonomy' => 'segmento', 'hide_empty' => false]);
      if ($segmentos) {
        echo '<div class="col-12 mb-2">',
                '<h4 class="mb-2 mb-md-3">Segmentos</h4>',
                  '<div class="form-group d-flex flex-wrap justify-content-md-start w-75">';
                    foreach ($segmentos as $segmento) {
                      echo '<div class="custom-control custom-checkbox col-md-6">',
                              '<input class="custom-control-input" name="'.$segmento->term_id.'" type="checkbox" value="'.$segmento->term_id.'" id="'.$segmento->term_id.'">',
                              '<label class="custom-control-label normal" for="'.$segmento->term_id.'">'.$segmento->name.'</label>',
                            '</div>';
                    }
                    
        echo      '</div>',
              '</div>';
      }
    ?>

    <div class="col-12 mb-2">
      <h4 class="mb-2 mb-md-3">Territórios</h4>
      <?php 
        $terms = get_terms(['taxonomy' => 'territorios', 'hide_empty' => false]);
        if ($terms) {
          echo '<div class="facetwp-facet facetwp-facet-territorios facetwp-type-fselect" data-name="territorios" data-type="fselect">';
          echo '<select class="fSelect--territorios multiple" multiple="multiple">';
            foreach ($terms as $term ) {              
              echo '<option value="'.$term->term_id.'">'.$term->name.'</option>';
            }
          echo '</select  >';
          echo '</div>';
        }
      ?>
    </div>

    <div class="col-md-6 d-flex align-items-center">
      <div class="form-group w-100">
        <div class="custom-control custom-checkbox flex-fill ">
          <input class="custom-control-input" name="terms" type="checkbox" value="terms" id="terms">
          <label class="custom-control-label normal" for="terms">Aceito os Termos de uso</label>
        </div>
      </div>
    </div>

    <div class="col-md-6 d-none align-items-end btn-update">
      <input type="hidden" name="action" value="ajax_edit_notification" />
      <?php wp_nonce_field('ajax-notification-nonce', 'security'); ?>

      <button type="submit" class="btn btn--classic submit_button w-100 mb-4 no-shadow">Salvar preferências <i class="icon icon-angle-right ml-5"></i></button>
    </div>
  </div>

</form>