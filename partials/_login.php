<form id="form-login" class="form-classic form-classic--account mb-classic" method="post" role="form">
  <p class="status"></p>

  <div class="form-group mb-2 mb-xl-4">
    <label for="username" class="normal">E-mail</label>
    <input id="username" class="form-control" type="text" name="username">
  </div>

  <div class="form-group mb-3 mb-xl-5">
    <label for="password" class="normal">Senha</label>
    <input id="password" type="password" name="password" type="password" class="form-control">
  </div>
  
  <input type="hidden" name="action" value="ajax_login" />
  <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>

  <button type="submit" class="btn btn--classic submit_button w-100 mb-4 no-shadow">Entrar <i class="icon icon-angle-right ml-5"></i></button>

  <div class="form-group d-flex justify-content-md-between">
    <div class="custom-control custom-checkbox flex-fill">
      <input class="custom-control-input" name="rememberme" type="checkbox" value="forever" id="rememberme">
      <label class="custom-control-label normal" for="rememberme">Mantenha-me conectado</label>
    </div>
    <a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Esqueci minha senha</a>
  </div>
</form>

<h3 class="s-title s-title--small mb-3">Ainda não possui conta?</h3>
<a href="<?php echo get_permalink(190); ?>" class="btn btn--classic btn--classic__ter w-100 no-shadow">Cadastre-se Gratuitamente</a>