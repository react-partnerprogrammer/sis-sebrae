<div class="h-page h-page--clean d-flex align-items-center justify-content-center">
  <?php
    if (is_home() || is_single()) :
      echo '<h1>Blog</h1>';

    else :
      the_title('<h1>', '</h1>');
    endif;
  ?>
</div>