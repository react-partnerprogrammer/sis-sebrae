<?php 
  $term_object = get_queried_object(); 
  $taxonomy = get_taxonomy($term_object->taxonomy);
  $icon    = get_field('icon_category', 'term_'.$term_object->term_id);  
 ?>
<?php // if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); } ?>
  <p id="breadcrumbs">
    <span>
      <span>
        <a href="<?php echo site_url('/') ?>">Home</a> &gt;         
        <?php if ($term_object->taxonomy == 'segmento'): ?>
        <a href="<?php echo site_url('/segmentos') ?>">Segmentos</a> 
        <?php else: ?>
        <span class="breadcrumb_last" aria-current="page"><?php echo $taxonomy->label ?></span>
        <?php endif; ?>        
      </span>
    </span>
  </p>
  <h2 class="s-title s-title--has-icon">
    <?php if ($icon) : ?>
      <div class="icon"><img src="<?php echo $icon['url']; ?>" alt="<?php single_cat_title(); ?>" class="img-fluid"></div>
    <?php endif; ?>  
    <?php single_cat_title(); ?>
  </h2>