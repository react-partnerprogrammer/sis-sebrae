<div class="container">
  <h1 class="sr-only">Filtro de busca</h1>
  <div class="row">
    <div class="col-11 mx-auto">
      <div class="block--search has-shadow">
        <h2 class="s-title">Encontre os melhores conteúdos para você:</h2>

        <div class="form-row align-items-end">
          <div class="col"><p>SETOR</p><?php echo facetwp_display( 'facet', 'select_setores' ); ?></div>
          <div class="col"><p>SEGMENTOS</p><?php echo facetwp_display( 'facet', 'select_segmento' ); ?></div>
          <div class="col"><p>TEMA & MERCADO</p><?php echo facetwp_display( 'facet', 'select_temas' ); ?></div>
          <div class="col-auto"><button class="btn btn--classic has-shadow fwp-submit__home" data-href="/busca/">Buscar</button></div>
        </div>

        <div class="d-none"><?php echo facetwp_display( 'template', 'results' ); ?></div>
      </div>
    </div>
  </div>
</div>