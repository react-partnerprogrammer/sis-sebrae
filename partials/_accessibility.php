<ul class="accessibility">
  <li>
    <div class="dropdown">
    <button class="libras" type="button" id="dropdownLibras" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="icon icon-accessibility"></i>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownLibras">
      <img src="<?php images_url('vlibras.gif'); ?>" alt="O conteúdo desse site pode ser acessível em Libras usando o Vlibras." class="d-block mx-auto mb-3 img-fluid">
      <p>O conteúdo desse site pode ser acessível em Libras usando o</p>
      <p><a href="http://www.vlibras.gov.br" target="_blank">VLibras.</a></p>
    </div>
  </div>
  </li>
  <li><button class="font-small"><small>A</small></button></li>
  <li><button class="font-normal">A</button></li>
  <li><button class="font-large"><strong>A</strong></button></li>
  <li><button class="contrast"><i class="icon icon-contrast"></i></button></li>
  <li>Acessibilidade</li>
</ul>