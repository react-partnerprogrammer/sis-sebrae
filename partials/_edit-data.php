<?php 
  $user = wp_get_current_user(); 
  $user_id = $user->ID;
?>
<form id="form-edit" class="form-classic form-classic--account mb-classic block-inputs" method="post" role="form">
  <p class="status"></p>

  <div class="row">
    <div class="col-12">
      <h4 class="mb-2 mb-md-4">Dados pessoais</h4>
    </div>
    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="cpf" class="normal">CPF:</label>
        <input id="cpf" class="form-control mask-cpf" type="text" name="cpf" value="<?php the_field('cpf_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="phone" class="normal">Telefone:</label>
        <input id="phone" class="form-control mask-phone" type="tel" name="phone" value="<?php the_field('phone_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="username" class="normal">E-mail:</label>
        <input id="username" class="form-control" type="text" name="username" value="<?php echo $user->user_login; ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-3 mb-xl-5 position-relative">
        <label for="password" class="normal">Senha:</label>
        <input id="password" type="password" name="password" class="form-control">
        <span class="icon icon-visibility toggle-password"></span>
      </div>
    </div>

    <div class="col-12">
      <h4 class="mb-2 mb-md-4">Dados da empresa</h4>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="enterprise" class="normal">Tipo de empreendimento:</label>
        <input id="enterprise" class="form-control" type="text" name="enterprise" value="<?php the_field('enterprise_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="register" class="normal">Tipo de registro:</label>
        <input id="register" class="form-control" type="text" name="register" value="<?php the_field('register_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="cargo" class="normal">Cargo na empresa:</label>
        <input id="cargo" class="form-control" type="text" name="cargo" value="<?php the_field('role_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="number" class="normal">Número:</label>
        <input id="number" class="form-control" type="number" name="number" value="<?php the_field('number_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="date_open" class="normal">Data de abertura:</label>
        <input id="date_open" class="form-control" type="text" name="date_open" value="<?php the_field('dateopen_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="social_name" class="normal">Razão social:</label>
        <input id="social_name" class="form-control" type="text" name="social_name" value="<?php the_field('socialname_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group mb-2 mb-xl-4">
        <label for="cnae" class="normal">CNAE:</label>
        <input id="cnae" class="form-control" type="text" name="cnae" value="<?php the_field('cnae_userdata', 'user_'.$user_id ); ?>">
      </div>
    </div>

    <div class="col-md-6 align-items-end btn-update">
      <input type="hidden" name="action" value="ajax_edit" />
      <input type="hidden" name="ID" value="<?php echo $user_id; ?>" />
      <?php wp_nonce_field('ajax-edit-nonce', 'security'); ?>

      <button type="submit" class="btn btn--classic submit_button w-100 mb-4 no-shadow">Atualizar cadastro <i class="icon icon-angle-right ml-5"></i></button>
    </div>
  </div>

</form>