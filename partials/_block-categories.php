<?php if ( have_rows('blocks_categories') ) : ?>
  <div class="container block--categories">
    <h1 class="sr-only">Categorias</h1>
    <?php $i=1; while( have_rows('blocks_categories') ) : the_row(); ?>
      <h2 class="s-title s-title--has-line"><?php the_sub_field('title_block_category'); ?></h2>

      <?php if ( have_rows('links_block_category') ) : ?>
      
        <div class="row mb-classic <?php echo ($i==1) ? 'first-row' : 'second-row' ; ?>">
          <?php while( have_rows('links_block_category') ) : the_row(); ?>
            <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
              <a href="<?php echo get_sub_field('category_block')['url']; ?>">
                <div class="media box-category has-shadow">
                  <img src="<?php the_sub_field('icon_block'); ?>" alt="<?php echo get_sub_field('category_block')['title']; ?>" class="img-fluid mr-4">
                  <div class="media-body">
                    <?php echo get_sub_field('category_block')['title']; ?>
                  </div>
                </div>
              </a>
            </div>
          <?php endwhile; ?>
        </div>
      
      <?php endif; ?>
      
    <?php $i++; endwhile; ?>

    <div class="row justify-content-center mb-classic">
      <a href="<?php echo get_permalink(); ?>" class="btn btn--classic">Veja mais setores <i class="icon icon-angle-right ml-3"></i></a>
    </div>
  </div>
<?php endif; ?>
