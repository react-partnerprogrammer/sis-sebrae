<?php user_notification(); ?>

<main id="content" role="main">
  <section>
    <?php if (
        is_archive('category') || 
        is_page_template('templates/page-busca.php') || 
        is_page_template('templates/page-territorios.php') || 
        is_single() && !is_tax('segmento') && !is_tax('setores') && !is_tax('temas_e_mercados')
        ) : ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-lg-4 col-xl-3 col-offcanvas col-offcanvas--sidebar <?php if (is_page_template('templates/page-busca.php')) echo 'active'; ?>">
          <?php get_template_part('partials/_filter'); ?>
        </div>

      <div class="col-lg-12 col-offcanvas col-offcanvas--content mt-3 <?php if (is_page_template('templates/page-busca.php')) echo 'active'; ?>">
    <?php endif; ?>
          