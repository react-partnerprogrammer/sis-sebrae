<?php 
  $agenda_id = 9;
  // Pega as categorias registradas
  $categorias = get_categories(['exclude' => $agenda_id]);   
  
  // Carrega o termo buscado se for archive
  $term = is_archive() ? get_queried_object() : false;


  if ($categorias) {
    
    // Para cada categegoria, cria um loop
    foreach ($categorias as $cat ) {
      // Pula as categorias vazias
      if (!$cat->count) continue;
      
      // Exibe os últimos 3 posts de cada categoria                                
      $args = ['cat' => $cat->term_id, 'posts_per_page' => 3];

      // Se for páginas de arquivo, filtro pelo termo
      if ($term) {                
        $args[$term->taxonomy] = $term->slug;        
      }
      
      $query = new WP_Query($args); 
      if ($query->have_posts()):
      
        // Imprime o título da categoria
        echo '<div id="box-'.$cat->slug.'" class="col-12 mt-4"> <h2 class="s-title mb-4">'.$cat->name.'</h2></div>';
        
        while ($query->have_posts()): $query->the_post();
          echo '<div class="col-md-4">';
            get_template_part( 'contents/_loop' );
          echo '</div>';
        endwhile;
        
        if ($term && $query->found_posts > 3) : ?>
          <div class="col-md-12 d-flex justify-content-center">
            <div class=" mb-classic">
              <a href="<?php echo get_busca_link([$term, $cat]); ?>" class="btn btn--classic">VEJA MAIS <i class="icon icon-angle-right ml-3"></i></a>
            </div>  
          </div>
        <?php 
        endif;
      endif; wp_reset_postdata();
                      
    }

  }
?>