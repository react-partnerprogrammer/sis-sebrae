<div class="h-page d-flex align-items-center justify-content-center mb-classic"
  <?php 
    if ( is_single() || is_home() ) { 
      thumbnail_bg_posts('header-full'); 
    } else { 
      thumbnail_bg( 'header-full' ); } 
  ?>
>
  <?php
    if (is_home() || is_single()) :
      echo '<h1>Blog</h1>';

    else :
      the_title('<h1>', '</h1>');
    endif;
  ?>
</div>