<?php 
  $queriedObject = get_queried_object(); 
  $thumb   = get_field('image_category', 'term_'.$queriedObject->term_id);
  $gallery = get_field('slider_category', 'term_'.$queriedObject->term_id);
 ?>

<?php if ($gallery || $thumb) : ?>
  <div class="col-md-9 mb-classic mb-classic--big">
    <div class="h-page h-page--term">
      <?php
        if ($gallery) {
          echo '<div class="h-page--slider slick">';
            foreach ($gallery as $image) {
              echo '<div class="d-block">',
                      '<img src="'.$image['sizes']['post-large'].'" class="img-fluid w-100">',
                      '<div class="mask"></div>',
                    '</div>';
            }
          echo '</div>';
        }
        elseif ($thumb) {
          echo '<img src="'.$thumb['sizes']['post-large'].'" alt="'.$term->name.'" class="img-fluid mb-5 w-100">';
        }
      ?>
    </div>
    <?php
      if ($queriedObject->description) {
        echo apply_filters('the_content', $queriedObject->description);
      }
    ?>
  </div>  
<?php endif; ?> 