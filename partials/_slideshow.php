<!-- slideshow -->
<?php if( have_rows('slideshow',get_option( 'page_on_front' )) ): ?>
  <div class="slideshow" role="banner">
    <h1 class="sr-only">Slideshow</h1>
    
    <div class="banner">
      <?php
        while ( have_rows('slideshow',get_option( 'page_on_front' )) ) : the_row();
        $image = get_sub_field('image_slider');
        $url = $image['url'];
        $size = 'slide';
        $thumb = $image['sizes'][ $size ];
      ?>
        <div>
          <div class="banner--content" style="background-image: url(<?php echo $thumb; ?>);">
            <?php
              if( get_sub_field('title_slider') ):
                echo '<h4>' . get_sub_field('title_slider') . '</h4>';
              endif;
              if( get_sub_field('desc_slider') ):
                echo '<p>' . get_sub_field('desc_slider') . '</p>';
              endif;
              if( get_sub_field('btn_slider') ):
                echo '<a href="' . get_sub_field('btn_slider')['url'] . '">'. get_sub_field('btn_slider')['title'] .'</a>';
              endif;
            ?>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
<?php endif; ?>
<!-- slideshow end -->