<div class="filter mt-3">
  <div class="filter--toggle"><i class="icon icon-angle-left"></i></div>
    <div class="form-group box-search pr-2">
      <div class="box-search-header">
        <label class="box-search-header__title" for="search">Buscar por</label>
        <label class="facetwp-clear" onclick="FWP.reset()" >LIMPAR FILTROS</label>
      </div>
      <?php echo facetwp_display( 'facet', 'search' ); ?>
    </div>

    <?php 
      form_group('Estou buscando', 'category', 'two');
      form_group('Setores da economia', 'setores', 'two');
      form_group('Temas e mercados', 'temas', 'one');
      form_group('Segmentos', 'segmento', 'one');
      form_group('Territórios', 'territorios', 'one');
    ?>

    <?php 
      if (is_page_template('templates/page-busca.php')) {
        echo '<button class="btn btn--classic w-100 mt-3" onclick="FWP.refresh()"><span>Buscar</span></button>';
      } else {
        echo '<button data-href="/busca/" class="fwp-submit btn btn--classic w-100 mt-3" onclick="FWP.refresh()"><span>Buscar</span></button>';
      }
    ?>
      
</div>