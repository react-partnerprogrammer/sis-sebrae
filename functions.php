<?php

// setup framework functions
require_once ('functions/setup.php');
require_once ('functions/wp-admin.php');

// general functions, filters and hooks
require_once ('functions/bfriend.php');
require_once ('functions/bfriend-acf.php');
require_once ('functions/bfriend-functions.php');
require_once ('functions/wp-ajax.php');

// wordpress navwalker
require_once ('functions/wp_bootstrap_navwalker.php');

// register custom post type
// require_once ('includes/cpt-type.php');


/**
 * Create a link with the params to match with Facet
 *
 * @param array $terms
 * @return void
 */ 
function get_busca_link($terms = []) {
  
  $return = get_site_url(null, '/busca');

  if ($terms) {
    $params = [];
    foreach ($terms as $term) {
      
      if ($term->taxonomy == 'temas_e_mercados') {
        $slug = 'temas';
      } else {
        $slug = $term->taxonomy;
      }

      $params['_'.$slug] = $term->slug;
    }

    $return .= '?' . http_build_query($params);
  }  
  return $return;
}


// Disable admin bar for users
add_action('after_setup_theme', 'remove_admin_bar'); 
function remove_admin_bar() {
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}

// Disable wp-admin for subscribers
function my_custom_catabilities(){     
  $subscriber = get_role( 'subscriber' );
  $subscriber->remove_cap( 'read' );
}
add_action( 'init', 'my_custom_catabilities' );

function get_my_login_url($extraArgs = '') {  
  return get_permalink(188). '?'. http_build_query(['redirect_to' => get_permalink() . $extraArgs]);
}
/**
* Customize the Favorites Button CSS Classes
*/
add_filter( 'favorites/button/css_classes', 'custom_favorites_button_css_classes', 10, 3 );
function custom_favorites_button_css_classes($classes, $post_id, $site_id)
{
  if (get_post_type($post_id) == 'post') {
    $classes .= ' btn btn--classic btn--classic__favorite mr-3';    
  } else {
    $classes .= ' btn--remove';        
  }
	return $classes;
}

add_filter( 'favorites/authentication_redirect_url', 'redirect_favorite_button_url');
function redirect_favorite_button_url() {
  return get_my_login_url('?favoritar=1');
}


/**
 * Add Logout To logout Dropdown Menu
 */ 
add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );
function wti_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'logged-user') {
     $items .= '<li class=""><a href="'. wp_logout_url() .'">'. __("Sair") .'</a></li>';
   }
   return $items;
}

// Allow FacetWP on WP_Query
// https://facetwp.com/documentation/templates/wp-query/
add_filter( 'facetwp_is_main_query', function( $bool, $query ) {
  return ( true === $query->get( 'facetwp' ) ) ? true : $bool;
}, 10, 2 );



/**
 * Return the banners based on the ACF Field passed
 *
 * @return void
 */
function get_banners($field, $object) {
  ob_start();
  if ( have_rows($field, $object) ) :  ?>
      <div class="h-archive d-none d-md-block">
        <div class="h-archive--slider slick">
          <?php while( have_rows($field, $object) ) : the_row(); ?>
        
            <div class="h-archive--slider--container">
              <img  class="img-fluid" src="<?php echo get_sub_field('image_sc')['sizes']['banner']; ?>" alt="<?php echo get_sub_field('link_sc')['title']; ?>" />
              <?php if( get_sub_field('link_sc') ) : ?>
                <h2><a href="<?php echo get_sub_field('link_sc')['url']; ?>"><?php echo get_sub_field('link_sc')['title']; ?></a></h2>
              <?php endif; ?>
            </div>
        
          <?php endwhile; ?>
        </div>
      </div>
  <?php endif;
  $return = ob_get_contents();
  ob_end_clean();
  return $return;
}


add_filter( 'body_class', function( $classes ) {
  if (is_page_template('templates/page-busca.php')) {
    $classes = array_merge( $classes, array( 'active' ) );
  }
  return $classes;
} );